#-------------------------------------------------
#
# Project created by QtCreator 2016-09-17T16:38:49
#
#-------------------------------------------------

QT       += testlib
QT       -= gui

TARGET = tst_benchmarktest

CONFIG += C++11
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += tst_benchmarktest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

include( ../deploy.pri )
DESTDIR = $$PROJECT_ROOT_DIRECTORY/deploy/

# FrameGenerator
LIBS += -L$$DESTDIR/ -lFrameGenerator

INCLUDEPATH += $$PWD/../FrameGenerator
DEPENDPATH += $$PWD/../FrameGenerator

# method_simple
INCLUDEPATH += $$PWD/../method_simple
DEPENDPATH += $$PWD/../method_simple

# method_memcpy
INCLUDEPATH += $$PWD/../method_memcpy
DEPENDPATH += $$PWD/../method_memcpy

# method_simple_openmp
INCLUDEPATH += $$PWD/../method_simple_openmp
DEPENDPATH += $$PWD/../method_simple_openmp
