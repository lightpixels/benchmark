#include <QString>
#include <QtTest>

#include "framegenerator.h"
extern "C" {
#include "doublebuffering.h"
}

class BenchmarkTest : public QObject
{
    Q_OBJECT

public:
    BenchmarkTest();

private Q_SLOTS:
    void raspi2fb_data();
    void raspi2fb();
};

BenchmarkTest::BenchmarkTest()
{
}

void BenchmarkTest::raspi2fb_data()
{
    QTest::addColumn< QString >("library");
    QTest::addColumn< size_t >("nPixels");
    QTest::addColumn< short unsigned int >("nFrames");
    QTest::addColumn< short unsigned int >("nUpdatePercentage");
    QTest::addColumn< short unsigned int >("nUpdateChunkSize");

    // Parameters #1
    size_t nPixels = 320*240;
    short unsigned int nFrames = 100;
    short unsigned int nUpdatePercentage = 20;
    short unsigned int nUpdateChunkSize = 50;

    QTest::newRow("MethodSimple - 100 frames")       << "MethodSimple" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;
    QTest::newRow("MethodMemcpy - 100 frames")       << "MethodMemcpy" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;
    QTest::newRow("MethodSimpleOpenMP - 100 frames") << "MethodSimpleOpenMP" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;

    // Parameters #2
    nFrames = 500;
    nUpdatePercentage = 20;
    nUpdateChunkSize = 50;

    QTest::newRow("MethodSimple - 500 frames")       << "MethodSimple" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;
    QTest::newRow("MethodMemcpy - 500 frames")       << "MethodMemcpy" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;
    QTest::newRow("MethodSimpleOpenMP - 500 frames") << "MethodSimpleOpenMP" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;

    // Parameters #3
    nFrames = 1000;
    nUpdatePercentage = 20;
    nUpdateChunkSize = 50;

    QTest::newRow("MethodSimple - 1000 frames")       << "MethodSimple" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;
    QTest::newRow("MethodMemcpy - 1000 frames")       << "MethodMemcpy" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;
    QTest::newRow("MethodSimpleOpenMP - 1000 frames") << "MethodSimpleOpenMP" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;

    /*
    // Parameters #4
    nFrames = 500;
    nUpdatePercentage = 20;
    nUpdateChunkSize = 1;

    QTest::newRow("DoubleBufferingCopyMethod - chunk size = 1")   << "DoubleBufferingCopyMethod" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;
    QTest::newRow("DoubleBufferingMemcpyMethod - chunk size = 1") << "DoubleBufferingMemcpyMethod" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;

    // Parameters #5
    nFrames = 500;
    nUpdatePercentage = 20;
    nUpdateChunkSize = 5;

    QTest::newRow("DoubleBufferingCopyMethod - chunk size = 5")   << "DoubleBufferingCopyMethod" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;
    QTest::newRow("DoubleBufferingMemcpyMethod - chunk size = 5") << "DoubleBufferingMemcpyMethod" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;

    // Parameters #6
    nFrames = 500;
    nUpdatePercentage = 20;
    nUpdateChunkSize = 10;

    QTest::newRow("DoubleBufferingCopyMethod - chunk size = 10")   << "DoubleBufferingCopyMethod" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;
    QTest::newRow("DoubleBufferingMemcpyMethod - chunk size = 10") << "DoubleBufferingMemcpyMethod" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;

    // Parameters #7
    nFrames = 500;
    nUpdatePercentage = 20;
    nUpdateChunkSize = 20;

    QTest::newRow("DoubleBufferingCopyMethod - chunk size = 20")   << "DoubleBufferingCopyMethod" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;
    QTest::newRow("DoubleBufferingMemcpyMethod - chunk size = 20") << "DoubleBufferingMemcpyMethod" << nPixels << nFrames << nUpdatePercentage << nUpdateChunkSize;
    */
}

void BenchmarkTest::raspi2fb()
{
    QFETCH(QString,            library);
    QFETCH(size_t,             nPixels);
    QFETCH(short unsigned int, nFrames);
    QFETCH(short unsigned int, nUpdatePercentage);
    QFETCH(short unsigned int, nUpdateChunkSize);

    FrameGenerator frameGenerator(nPixels,nFrames,nUpdatePercentage,nUpdateChunkSize);
    frameGenerator.generateFrames();

    //! The destination FrameBuffer:
	uint16_t* pixelsTo = new uint16_t[nPixels];

    //! The double buffering method needs two extra buffers:
    uint16_t* pixelsFront = new uint16_t[nPixels];
    uint16_t* pixelsBack  = new uint16_t[nPixels];

    typedef void (*DoubleBufferingCopyFunction)(uint16_t*, uint16_t*, unsigned long int, uint16_t*, uint16_t*);

    DoubleBufferingCopyFunction doubleBufferingCopy =
            (DoubleBufferingCopyFunction) QLibrary::resolve(library, "doubleBufferingCopy");

    bool doubleBufferingCopyFound = (doubleBufferingCopy != 0);

    if(!doubleBufferingCopyFound) {
        delete[] pixelsFront;
        delete[] pixelsBack;
        delete[] pixelsTo;
    }

    QVERIFY2(doubleBufferingCopyFound,"The doubleBufferingCopy function has not been found.");

    QBENCHMARK {
        uint16_t* frame = nullptr;
        while((frame = frameGenerator.read())) {
            doubleBufferingCopy(frame, pixelsTo, nPixels, pixelsFront, pixelsBack);
        }
    }

    //! Deallocate buffers:
    delete[] pixelsFront;
    delete[] pixelsBack;
	delete[] pixelsTo;
}

QTEST_APPLESS_MAIN(BenchmarkTest)

#include "tst_benchmarktest.moc"
